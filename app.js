

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongodb = require('mongodb');//injecting database onto backend.
const assert = require('assert');
var cors = require('cors')

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const config = require('./config');

const PORT = 4000;

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
var originsWhitelist = [
  'http://localhost:8100' // this is my front-end url for development
]
var corsOptions = {
  origin: function (origin, callback) {
    var isWhitelisted = originsWhitelist.indexOf(origin) !== -1
    callback(null, isWhitelisted)
  },
  credentials: true
}
// here is the magic
app.use(cors(corsOptions))
var db;

mongodb.MongoClient.connect(config.dbString, { useNewUrlParser: true }, function (err, database) {
	assert.equal(null, err);

	db=database.db('ECTEST');
  require('./nextroute')(app, db);

	console.log('Database connected succesfully');

})
app.get('/', (req, res) => {
	res.send('Hello!  ' + PORT)
})
app.listen(PORT, (err) => {
	assert.equal(null, err);
	console.log('App started succesfully in port : ' + PORT);
})
// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
