
/* we need to require all the pages we create on "routes.js" file so that a link can be 
  established. If we do not need this the  request flow will be interrupted */

module.exports = (app, db) => {
  const moment = require('moment');
  var ObjectId = require('mongodb').ObjectID;
  const dateformat = require('dateformat');
  const AUTH_SIGNUP = 'Pauth_Signup'; //expendibledetails=collection name
  const CATAGERY = 'PCategory';
  const CUSTOMER = 'Pcustomer';
  

  const commonData = require('../common/commonData');
  var todayDate = new Date()
  var year = todayDate.getFullYear();
  var month = todayDate.getMonth();
  var day = todayDate.getDate();
  var endDate = new Date(year + 1, month, day);

  //signupAPI
  app.post('/signup_pinto', function (req, res, err) {
    var data = req.body;
    var tdate=new Date();
    var formatedDate =dateformat(tdate, 'dd-mm-yyyy');
    console.log(data,"data here to make da")

    if((!data.name)||(!data.email)||(!data.password)||(!data.og_name)||(!data.phone)){
      res.json({ status: false, message: commonData.resMessages().msg1 });
    }else{
      var datas = {
        "name": data.name,
        "email": data.email,
        "password": data.password,
        "organization": data.og_name,
        "phone": data.phone,
        "createddate": formatedDate,
         "endDate":moment(tdate).add(1, 'year').format("DD-MM-YYYY"),
        "active": 1
      }
      db.collection(AUTH_SIGNUP).find({ "phone": data.phone }).toArray().then(data => {
        if (data.length) {
          res.json({ status: false, message: commonData.resMessages().msg6 });
        } else {
          db.collection(AUTH_SIGNUP).insert(datas, (err, result) => {
            if (err) res.json({ status: false, message: commonData.resMessages().msg2 });

            else res.json({ status: true, message: commonData.resMessages().msg5 });
          })
        }


      })

    }


  })
  //login
  app.post('/login_pinto', function (req, res, err) {
    var data = req.body;
    console.log(data,"data here da")
    if ((!data.phone) || (!data.password)) {
      res.json({ status: false, message: commonData.resMessages().msg1 });
    } else {
      db.collection(AUTH_SIGNUP).find({ "phone": data.phone, "password": data.password }).toArray().then(data => {
        if (data.length) {
          res.json({ status: true, message: commonData.resMessages().msg5, data: data });
        } else {
          res.json({ status: false, message: commonData.resMessages().msg7 });

        }
      })

    }

  })

   //addnew
  app.post('/add_new', function (req, res, err) {
    //add new function
function insertintoNew(data){
  db.collection(CUSTOMER).insertOne(data, (err, result) => {
  if (err) res.json({ status: false, message: commonData.resMessages().msg2 });
  else res.json({ status: true, message: commonData.resMessages().msg5 });
  })
}
    var data = req.body;
    var vechileNumber=data.vehiclenumber.replace(/\s+/g, '').toUpperCase()
    var tdate = new Date();
    var formatedDate = dateformat(tdate, 'dd-mm-yyyy');

    if ((!data.vehiclenumber)||(!data.phone)||(!data.name)||(!data.userId)) {
      res.json({ status: false, message: commonData.resMessages().msg1 });
    } else {
      if(data.vehicletype==true) enddate = moment(tdate).add(1, 'year').format("DD-MM-YYYY");
      else enddate=moment(tdate).add(6, 'month').format("DD-MM-YYYY");
      var insertdata = {

        "vehicleno": data.vehiclenumber.replace(/\s+/g, '').toUpperCase(),
        "phoneno": data.phone,
        "name": data.name,
        "userid": data.userId,
        "status": data.status,
        "vehicletype": data.vehicletype,
        "Manufacture": data.Manufacture,
        "ManufactureYear": data.ManufactureYear,
        "EngineNumber": data.EngineNumber,
        "issueddate": formatedDate,
        "enddate": enddate,
        "active":1

      }
      db.collection(CUSTOMER).find({ vehicleno: vechileNumber,"userid":data.userId }).toArray().then(doc => {
        if (doc.length > 0) {
          db.collection(CUSTOMER).remove({ "vehicleno": vechileNumber,"userid":data.userId })
          insertintoNew(insertdata)
        }
         else insertintoNew(insertdata)
        }
      );
    }
  })

//report

app.post('/Getreport',function(req, res, err){
var data =req.body;

let year=data.year;
let month=data.month;
let day=data.day;
let newdate=day+"-"+month+"-"+year;
if((!data.userId)){
  res.json({status: false,message: commonData.resMessages().msg1 });

}
else{
  if((data.userId)){
   
    var query={"userid": data.userId,"issueddate":newdate};
    db.collection(CUSTOMER).find(query).toArray().then(doc => {
      if (doc.length) {
        res.json({ status: true, message: commonData.resMessages().msg5, doc: doc });
      } else {
    
        res.json({ status: false, message: commonData.resMessages().msg8 });

      }
    })
    
  }
}
})


//activity
app.post('/expirydate', function (req, res, err) {
  var data = req.body;
   console.log(data,"data here");
  if ((!req.body.userId)) res.json({ status: false, message: commonData.resMessages().msg1 });
  else {
        var tdate=new Date();
    var formatedDate =dateformat(tdate, 'dd-mm-yyyy');
      var query = { "userid": req.body.userId,enddate: formatedDate}
    db.collection(CUSTOMER).find(query).toArray().then(doc => {
      
      if (doc.length>0) {
        res.json({ status: true, message: commonData.resMessages().msg5, doc: doc });
      } else {
        res.json({ status: false, message: commonData.resMessages().msg8 });

      }
    })
  }
})



  // view all contact
  app.post('/all_student', function (req, res, err) {
    var data = req.body;
    console.log(data,"data here")
    if ((!req.body.userId))  res.json({ status: false, message: commonData.resMessages().msg1 });
     else {
      if ((req.body.userId) ) {
        var query = { "userid": req.body.userId }

     
      db.collection(CUSTOMER).find(query).toArray().then(doc => {
        console.log(doc)
        if (doc.length>0) {
          res.json({ status: true, message: commonData.resMessages().msg5, doc: doc });
        } else {
          res.json({ status: false, message: commonData.resMessages().msg8 });

        }
      })
    }
    }
  })

}

